require('dotenv').config();
const fs = require('fs');
const express = require('express');
const app = express();
const http = require('http').createServer(app);

const socketClient = require('socket.io-client');

const knex = require('knex')({
    client: 'mysql',
    connection: {
        connectionLimit: 100,
        port: process.env.DB_PORT,
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME
    },
    pool: {
        min: 0,
        max: 30,
    }
});

knex('iba_devices')
    .select('id', 'label', 'mac_address')
    .then(function(rows) {
        launchConnections(rows);
    })
    .catch(function(err) {
        console.log(err);
    });

function launchConnections(rows) {
    var clients = [];
    var connectionInterval = 50;
    var numberOfUsersToConnect = rows.length;
    var connections = 0;
    var connectionTimestampSum = 0;
    var nbDevices = 0;
    var p = new Promise(function(resolve, reject) {
        rows.forEach(function(row) {
            nbDevices++;
            (function (index) {
                setTimeout(function () {
                    var time = Date.now();
                    var client = socketClient(process.env.SERVER);
                    client.on('connect', function() {
                        connections++;
                        let id = connections;
                        setTimeout(() => {
                            client.emit('assets:downloaded', id);
                        }, 6000);
                        client.on('disconnect', () => {
                            console.log(id, "Déconnecté");
                        });
                        var connectionTime = Date.now() - time;
                        connectionTimestampSum += connectionTime;

                        console.log((connections) + "/" + numberOfUsersToConnect + " connected! (" + (connectionTime)+ "ms)");
                        if(connectionTime > 1200) {
                            console.log(connectionTime+" ms !!");
                        }
                        client.emit('login', row);
                        clients.push(client);

                        //On stoppe à la fin de la boucle
                        if (index == numberOfUsersToConnect) {
                            resolve();
                        }
                    });
                }, index * connectionInterval);
            })(nbDevices);
        }, this);
    })
    .then(function() {
        console.log("Average connect time: " + Math.round(connectionTimestampSum / connections) + " ms.");
        console.log(connections+" total connections in "+connectionTimestampSum+"ms");
    });
}